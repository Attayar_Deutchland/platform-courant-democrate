<?php

$factory->define(App\State::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "lattitude" => $faker->randomFloat(2, 1, 100),
        "longitude" => $faker->randomFloat(2, 1, 100),
        "country_id" => factory('App\Country')->create(),
    ];
});
