<?php

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        "firstname" => $faker->name,
        "lastname" => $faker->name,
        "date_of_birthday" => $faker->date("Y-m-d", $max = 'now'),
        "adresse" => $faker->name,
        "date_inscription" => $faker->date("Y-m-d", $max = 'now'),
        "sexe" => collect(["femme","homme",])->random(),
        "status" => collect(["etudiant","employee","chomeur",])->random(),
        "user_id" => factory('App\User')->create(),
    ];
});
