<?php

$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "active" => 0,
        "date_beginn" => $faker->date("Y-m-d", $max = 'now'),
        "date_end" => $faker->date("Y-m-d", $max = 'now'),
        "country_id" => factory('App\Country')->create(),
        "state_id" => factory('App\State')->create(),
        "city_id" => factory('App\City')->create(),
    ];
});
