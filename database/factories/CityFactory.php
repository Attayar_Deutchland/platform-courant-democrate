<?php

$factory->define(App\City::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "lattitude" => $faker->randomFloat(2, 1, 100),
        "longitude" => $faker->randomFloat(2, 1, 100),
        "state_id" => factory('App\State')->create(),
    ];
});
