<?php

$factory->define(App\Academic::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "date_beginn" => $faker->name,
        "date_end" => $faker->name,
    ];
});
