<?php

$factory->define(App\Experience::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "date_beginn" => $faker->date("Y-m-d", $max = 'now'),
        "date_end" => $faker->date("Y-m-d", $max = 'now'),
    ];
});
