<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5df42404ce4515df42404cc362CityStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('city_state');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('city_state')) {
            Schema::create('city_state', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id', 'fk_p_43580_43579_state_ci_5df41845d2b8e')->references('id')->on('cities');
                $table->integer('state_id')->unsigned()->nullable();
            $table->foreign('state_id', 'fk_p_43579_43580_city_sta_5df41845d21ac')->references('id')->on('states');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
