<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1576275916CadresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cadres', function (Blueprint $table) {
            if(Schema::hasColumn('cadres', 'logo')) {
                $table->dropColumn('logo');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cadres', function (Blueprint $table) {
                        $table->string('logo')->nullable();
                
        });

    }
}
