<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1576275552RolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            
if (!Schema::hasColumn('roles', 'description')) {
                $table->text('description')->nullable();
                }
if (!Schema::hasColumn('roles', 'logo')) {
                $table->string('logo')->nullable();
                }
if (!Schema::hasColumn('roles', 'active')) {
                $table->tinyInteger('active')->nullable()->default('0');
                }
if (!Schema::hasColumn('roles', 'date_beginn')) {
                $table->date('date_beginn')->nullable();
                }
if (!Schema::hasColumn('roles', 'date_end')) {
                $table->date('date_end')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('logo');
            $table->dropColumn('active');
            $table->dropColumn('date_beginn');
            $table->dropColumn('date_end');
            
        });

    }
}
