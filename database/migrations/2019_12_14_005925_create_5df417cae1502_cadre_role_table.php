<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df417cae1502CadreRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('cadre_role')) {
            Schema::create('cadre_role', function (Blueprint $table) {
                $table->integer('cadre_id')->unsigned()->nullable();
                $table->foreign('cadre_id', 'fk_p_43577_43574_role_cad_5df417cae15bf')->references('id')->on('cadres')->onDelete('cascade');
                $table->integer('role_id')->unsigned()->nullable();
                $table->foreign('role_id', 'fk_p_43574_43577_cadre_ro_5df417cae1605')->references('id')->on('roles')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cadre_role');
    }
}
