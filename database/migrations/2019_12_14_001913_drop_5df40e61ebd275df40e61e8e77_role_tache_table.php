<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5df40e61ebd275df40e61e8e77RoleTacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('role_tache');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('role_tache')) {
            Schema::create('role_tache', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id', 'fk_p_43574_43576_tache_ro_5df40195d8979')->references('id')->on('roles');
                $table->integer('tache_id')->unsigned()->nullable();
            $table->foreign('tache_id', 'fk_p_43576_43574_role_tac_5df40195d933e')->references('id')->on('taches');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
