<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5df4015c1682c5df4015c14502CadreTacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('cadre_tache');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('cadre_tache')) {
            Schema::create('cadre_tache', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('cadre_id')->unsigned()->nullable();
            $table->foreign('cadre_id', 'fk_p_43577_43576_tache_ca_5df3fc417812f')->references('id')->on('cadres');
                $table->integer('tache_id')->unsigned()->nullable();
            $table->foreign('tache_id', 'fk_p_43576_43577_cadre_ta_5df3fc417737c')->references('id')->on('taches');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
