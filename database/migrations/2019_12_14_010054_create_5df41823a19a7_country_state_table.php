<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df41823a19a7CountryStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('country_state')) {
            Schema::create('country_state', function (Blueprint $table) {
                $table->integer('country_id')->unsigned()->nullable();
                $table->foreign('country_id', 'fk_p_43578_43579_state_co_5df41823a1a70')->references('id')->on('countries')->onDelete('cascade');
                $table->integer('state_id')->unsigned()->nullable();
                $table->foreign('state_id', 'fk_p_43579_43578_country__5df41823a1ab6')->references('id')->on('states')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_state');
    }
}
