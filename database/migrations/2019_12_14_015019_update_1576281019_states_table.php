<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1576281019StatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('states', function (Blueprint $table) {
            if(Schema::hasColumn('states', 'country_id')) {
                $table->dropForeign('43579_5df4117adf3ad');
                $table->dropIndex('43579_5df4117adf3ad');
                $table->dropColumn('country_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('states', function (Blueprint $table) {
                        
        });

    }
}
