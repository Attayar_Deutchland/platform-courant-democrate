<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df41845d3a1eCityStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('city_state')) {
            Schema::create('city_state', function (Blueprint $table) {
                $table->integer('city_id')->unsigned()->nullable();
                $table->foreign('city_id', 'fk_p_43580_43579_state_ci_5df41845d3adc')->references('id')->on('cities')->onDelete('cascade');
                $table->integer('state_id')->unsigned()->nullable();
                $table->foreign('state_id', 'fk_p_43579_43580_city_sta_5df41845d3b1f')->references('id')->on('states')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_state');
    }
}
