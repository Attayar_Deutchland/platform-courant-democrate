<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5df40e62c9ccbRelationshipsToRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function(Blueprint $table) {
            if (!Schema::hasColumn('roles', 'cadre_id')) {
                $table->integer('cadre_id')->unsigned()->nullable();
                $table->foreign('cadre_id', '43574_5df3fc7cce743')->references('id')->on('cadres')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function(Blueprint $table) {
            
        });
    }
}
