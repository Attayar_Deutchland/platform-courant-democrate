<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5df411e957addRelationshipsToCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function(Blueprint $table) {
            if (!Schema::hasColumn('cities', 'state_id')) {
                $table->integer('state_id')->unsigned()->nullable();
                $table->foreign('state_id', '43580_5df411e87dc22')->references('id')->on('states')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function(Blueprint $table) {
            
        });
    }
}
