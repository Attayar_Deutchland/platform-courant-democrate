<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5df423f68999c5df423f6878e7CountryStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('country_state');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('country_state')) {
            Schema::create('country_state', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id', 'fk_p_43578_43579_state_co_5df418239fdaa')->references('id')->on('countries');
                $table->integer('state_id')->unsigned()->nullable();
            $table->foreign('state_id', 'fk_p_43579_43578_country__5df41823a076a')->references('id')->on('states');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
