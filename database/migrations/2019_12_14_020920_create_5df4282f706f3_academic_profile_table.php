<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df4282f706f3AcademicProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('academic_profile')) {
            Schema::create('academic_profile', function (Blueprint $table) {
                $table->integer('academic_id')->unsigned()->nullable();
                $table->foreign('academic_id', 'fk_p_43595_43597_profile__5df4282f707ae')->references('id')->on('academics')->onDelete('cascade');
                $table->integer('profile_id')->unsigned()->nullable();
                $table->foreign('profile_id', 'fk_p_43597_43595_academic_5df4282f7081d')->references('id')->on('profiles')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_profile');
    }
}
