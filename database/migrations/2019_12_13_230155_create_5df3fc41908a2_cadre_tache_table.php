<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df3fc41908a2CadreTacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('cadre_tache')) {
            Schema::create('cadre_tache', function (Blueprint $table) {
                $table->integer('cadre_id')->unsigned()->nullable();
                $table->foreign('cadre_id', 'fk_p_43577_43576_tache_ca_5df3fc4190961')->references('id')->on('cadres')->onDelete('cascade');
                $table->integer('tache_id')->unsigned()->nullable();
                $table->foreign('tache_id', 'fk_p_43576_43577_cadre_ta_5df3fc41909a5')->references('id')->on('taches')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cadre_tache');
    }
}
