<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df4282f72f5aExperienceProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('experience_profile')) {
            Schema::create('experience_profile', function (Blueprint $table) {
                $table->integer('experience_id')->unsigned()->nullable();
                $table->foreign('experience_id', 'fk_p_43596_43597_profile__5df4282f73014')->references('id')->on('experiences')->onDelete('cascade');
                $table->integer('profile_id')->unsigned()->nullable();
                $table->foreign('profile_id', 'fk_p_43597_43596_experien_5df4282f73060')->references('id')->on('profiles')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_profile');
    }
}
