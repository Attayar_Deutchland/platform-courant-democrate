<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1576277962RolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            if(Schema::hasColumn('roles', 'cadre_id')) {
                $table->dropForeign('43574_5df3fc7cce743');
                $table->dropIndex('43574_5df3fc7cce743');
                $table->dropColumn('cadre_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
                        
        });

    }
}
