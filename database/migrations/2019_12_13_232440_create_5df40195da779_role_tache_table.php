<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5df40195da779RoleTacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('role_tache')) {
            Schema::create('role_tache', function (Blueprint $table) {
                $table->integer('role_id')->unsigned()->nullable();
                $table->foreign('role_id', 'fk_p_43574_43576_tache_ro_5df40195da841')->references('id')->on('roles')->onDelete('cascade');
                $table->integer('tache_id')->unsigned()->nullable();
                $table->foreign('tache_id', 'fk_p_43576_43574_role_tac_5df40195da883')->references('id')->on('taches')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_tache');
    }
}
