<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5df417cbbcb9dRelationshipsToRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function(Blueprint $table) {
            if (!Schema::hasColumn('roles', 'country_id')) {
                $table->integer('country_id')->unsigned()->nullable();
                $table->foreign('country_id', '43574_5df4127849517')->references('id')->on('countries')->onDelete('cascade');
                }
                if (!Schema::hasColumn('roles', 'state_id')) {
                $table->integer('state_id')->unsigned()->nullable();
                $table->foreign('state_id', '43574_5df41278506cd')->references('id')->on('states')->onDelete('cascade');
                }
                if (!Schema::hasColumn('roles', 'city_id')) {
                $table->integer('city_id')->unsigned()->nullable();
                $table->foreign('city_id', '43574_5df412785773f')->references('id')->on('cities')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function(Blueprint $table) {
            
        });
    }
}
