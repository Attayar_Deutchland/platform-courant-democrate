<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1576281043CitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function (Blueprint $table) {
            if(Schema::hasColumn('cities', 'state_id')) {
                $table->dropForeign('43580_5df411e87dc22');
                $table->dropIndex('43580_5df411e87dc22');
                $table->dropColumn('state_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
                        
        });

    }
}
