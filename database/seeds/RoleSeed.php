<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'title' => 'Administrator (can create other users)', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 1, 'state_id' => null, 'city_id' => null,],
            ['id' => 2, 'title' => 'Simple user', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 1, 'state_id' => null, 'city_id' => null,],
            ['id' => 3, 'title' => 'أمين عام', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 1, 'state_id' => null, 'city_id' => null,],
            ['id' => 4, 'title' => 'أمين عام مساعد', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 1, 'state_id' => null, 'city_id' => null,],
            ['id' => 5, 'title' => 'أمين مال', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 1, 'state_id' => null, 'city_id' => null,],
            ['id' => 6, 'title' => 'كاتب عام بالمكتب الجهوي بألمانيا', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 86, 'state_id' => null, 'city_id' => null,],
            ['id' => 7, 'title' => 'كاتب عام مساعد بالمكتب الجهوي بألمانيا', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 86, 'state_id' => null, 'city_id' => null,],
            ['id' => 8, 'title' => 'أمين مال بالمكتب الجهوي بألمانيا', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 86, 'state_id' => null, 'city_id' => null,],
            ['id' => 9, 'title' => 'مكلف بالانخراطات بالمكتب الجهوي بألمانيا', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 86, 'state_id' => null, 'city_id' => null,],
            ['id' => 10, 'title' => 'عضو بالمكتب المكتب الجهوي بألمانيا', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 86, 'state_id' => null, 'city_id' => null,],
            ['id' => 11, 'title' => 'منخرط بالمكتب المكتب الجهوي بألمانيا', 'description' => null, 'logo' => null, 'active' => 0, 'date_beginn' => null, 'date_end' => null, 'country_id' => 86, 'state_id' => null, 'city_id' => null,],
        ];

        foreach ($items as $item) {
            \App\Role::create($item);
        }
    }
}
