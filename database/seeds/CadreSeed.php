<?php

use Illuminate\Database\Seeder;

class CadreSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'title' => 'المكتب السياسي', 'description' => null, 'date_beginn' => null, 'date_end' => null,  'active' => 1,],
            ['id' => 2, 'title' => 'المكتب التنفيذي', 'description' => null,  'date_beginn' => null, 'date_end' => null,  'active' => 1,],
            ['id' => 3, 'title' => 'المجلس الوطني', 'description' => null,  'date_beginn' => null, 'date_end' => null,  'active' => 1,],
            ['id' => 4, 'title' => 'المكتب الجهوي بألمانيا', 'description' => null, 'date_beginn' => null, 'date_end' => null,  'active' => 1,],
        ];

        foreach ($items as $item) {
            \App\Cadre::create($item);
        }
    }
}
