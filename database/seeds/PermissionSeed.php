<?php

use Illuminate\Database\Seeder;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'title' => 'user_management_access',],
            ['id' => 2, 'title' => 'permission_access',],
            ['id' => 3, 'title' => 'permission_create',],
            ['id' => 4, 'title' => 'permission_edit',],
            ['id' => 5, 'title' => 'permission_view',],
            ['id' => 6, 'title' => 'permission_delete',],
            ['id' => 7, 'title' => 'role_access',],
            ['id' => 8, 'title' => 'role_create',],
            ['id' => 9, 'title' => 'role_edit',],
            ['id' => 10, 'title' => 'role_view',],
            ['id' => 11, 'title' => 'role_delete',],
            ['id' => 12, 'title' => 'user_access',],
            ['id' => 13, 'title' => 'user_create',],
            ['id' => 14, 'title' => 'user_edit',],
            ['id' => 15, 'title' => 'user_view',],
            ['id' => 16, 'title' => 'user_delete',],
            ['id' => 22, 'title' => 'cadre_access',],
            ['id' => 23, 'title' => 'cadre_create',],
            ['id' => 24, 'title' => 'cadre_edit',],
            ['id' => 25, 'title' => 'cadre_view',],
            ['id' => 26, 'title' => 'cadre_delete',],
            ['id' => 27, 'title' => 'country_access',],
            ['id' => 28, 'title' => 'country_create',],
            ['id' => 29, 'title' => 'country_edit',],
            ['id' => 30, 'title' => 'country_view',],
            ['id' => 31, 'title' => 'country_delete',],
            ['id' => 32, 'title' => 'state_access',],
            ['id' => 33, 'title' => 'state_create',],
            ['id' => 34, 'title' => 'state_edit',],
            ['id' => 35, 'title' => 'state_view',],
            ['id' => 36, 'title' => 'state_delete',],
            ['id' => 37, 'title' => 'city_access',],
            ['id' => 38, 'title' => 'city_create',],
            ['id' => 39, 'title' => 'city_edit',],
            ['id' => 40, 'title' => 'city_view',],
            ['id' => 41, 'title' => 'city_delete',],
            ['id' => 42, 'title' => 'profile_access',],
            ['id' => 43, 'title' => 'profile_create',],
            ['id' => 44, 'title' => 'profile_edit',],
            ['id' => 45, 'title' => 'profile_view',],
            ['id' => 46, 'title' => 'profile_delete',],
            ['id' => 47, 'title' => 'experience_access',],
            ['id' => 48, 'title' => 'experience_create',],
            ['id' => 49, 'title' => 'experience_edit',],
            ['id' => 50, 'title' => 'experience_view',],
            ['id' => 51, 'title' => 'experience_delete',],
            ['id' => 52, 'title' => 'academic_access',],
            ['id' => 53, 'title' => 'academic_create',],
            ['id' => 54, 'title' => 'academic_edit',],
            ['id' => 55, 'title' => 'academic_view',],
            ['id' => 56, 'title' => 'academic_delete',],

        ];

        foreach ($items as $item) {
            \App\Permission::create($item);
        }
    }
}
