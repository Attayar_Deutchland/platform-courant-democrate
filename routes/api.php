<?php

Route::group(['prefix' => '/v1', 'middleware' => ['auth:api'], 'namespace' => 'Api\User_Management', 'as' => 'api.'], function () {
    Route::post('change-password', 'ChangePasswordController@changePassword')->name('auth.change_password');
    Route::apiResource('rules', 'RulesController', ['only' => ['index']]);
    Route::apiResource('permissions', 'PermissionsController');
    Route::apiResource('roles', 'RolesController');
    Route::apiResource('users', 'UsersController');
    Route::apiResource('cadres', 'CadresController');
    Route::apiResource('countries', 'CountriesController');
    Route::apiResource('states', 'StatesController');
    Route::apiResource('cities', 'CitiesController');
    Route::apiResource('academics', 'AcademicsController');
    Route::apiResource('experiences', 'ExperiencesController');
    Route::apiResource('profiles', 'ProfilesController');
});
