<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Profile
 *
 * @package App
 * @property string $firstname
 * @property string $lastname
 * @property string $photo
 * @property string $date_of_birthday
 * @property string $adresse
 * @property string $date_inscription
 * @property string $sexe
 * @property string $status
 * @property string $user
*/
class Profile extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    
    protected $fillable = ['firstname', 'lastname', 'date_of_birthday', 'adresse', 'date_inscription', 'sexe', 'status', 'user_id'];
    protected $appends = ['photo', 'photo_link'];
    protected $with = ['media'];
    

    public static function storeValidation($request)
    {
        return [
            'firstname' => 'max:191|required',
            'lastname' => 'max:191|required',
            'photo' => 'file|image|nullable',
            'date_of_birthday' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'adresse' => 'max:191|nullable',
            'date_inscription' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'sexe' => 'in:femme,homme|max:191|nullable',
            'status' => 'in:etudiant,employee,chomeur|max:191|nullable',
            'academic' => 'array|nullable',
            'academic.*' => 'integer|exists:academics,id|max:4294967295|nullable',
            'experience' => 'array|nullable',
            'experience.*' => 'integer|exists:experiences,id|max:4294967295|nullable',
            'user_id' => 'integer|exists:users,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'firstname' => 'max:191|required',
            'lastname' => 'max:191|required',
            'photo' => 'nullable',
            'date_of_birthday' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'adresse' => 'max:191|nullable',
            'date_inscription' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'sexe' => 'in:femme,homme|max:191|nullable',
            'status' => 'in:etudiant,employee,chomeur|max:191|nullable',
            'academic' => 'array|nullable',
            'academic.*' => 'integer|exists:academics,id|max:4294967295|nullable',
            'experience' => 'array|nullable',
            'experience.*' => 'integer|exists:experiences,id|max:4294967295|nullable',
            'user_id' => 'integer|exists:users,id|max:4294967295|required'
        ];
    }

    

    public function getPhotoAttribute()
    {
        return $this->getFirstMedia('photo');
    }

    /**
     * @return string
     */
    public function getPhotoLinkAttribute()
    {
        $file = $this->getFirstMedia('photo');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateOfBirthdayAttribute($input)
    {
        if ($input) {
            $this->attributes['date_of_birthday'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateOfBirthdayAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateInscriptionAttribute($input)
    {
        if ($input) {
            $this->attributes['date_inscription'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateInscriptionAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    public function academic()
    {
        return $this->belongsToMany(Academic::class, 'academic_profile')->withTrashed();
    }
    
    public function experience()
    {
        return $this->belongsToMany(Experience::class, 'experience_profile')->withTrashed();
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    
}
