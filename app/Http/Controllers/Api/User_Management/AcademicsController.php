<?php

namespace App\Http\Controllers\Api\User_Management;

use App\Academic;
use App\Http\Controllers\Controller;
use App\Http\Resources\Academic as AcademicResource;
use App\Http\Requests\Admin\StoreAcademicsRequest;
use App\Http\Requests\Admin\UpdateAcademicsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class AcademicsController extends Controller
{
    public function index()
    {


        return new AcademicResource(Academic::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('academic_view')) {
            return abort(401);
        }

        $academic = Academic::with([])->findOrFail($id);

        return new AcademicResource($academic);
    }

    public function store(StoreAcademicsRequest $request)
    {
        if (Gate::denies('academic_create')) {
            return abort(401);
        }

        $academic = Academic::create($request->all());



        return (new AcademicResource($academic))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateAcademicsRequest $request, $id)
    {
        if (Gate::denies('academic_edit')) {
            return abort(401);
        }

        $academic = Academic::findOrFail($id);
        $academic->update($request->all());




        return (new AcademicResource($academic))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('academic_delete')) {
            return abort(401);
        }

        $academic = Academic::findOrFail($id);
        $academic->delete();

        return response(null, 204);
    }
}
