<?php

namespace App\Http\Controllers\Api\User_Management;

use App\Experience;
use App\Http\Controllers\Controller;
use App\Http\Resources\Experience as ExperienceResource;
use App\Http\Requests\Admin\StoreExperiencesRequest;
use App\Http\Requests\Admin\UpdateExperiencesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class ExperiencesController extends Controller
{
    public function index()
    {


        return new ExperienceResource(Experience::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('experience_view')) {
            return abort(401);
        }

        $experience = Experience::with([])->findOrFail($id);

        return new ExperienceResource($experience);
    }

    public function store(StoreExperiencesRequest $request)
    {
        if (Gate::denies('experience_create')) {
            return abort(401);
        }

        $experience = Experience::create($request->all());



        return (new ExperienceResource($experience))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateExperiencesRequest $request, $id)
    {
        if (Gate::denies('experience_edit')) {
            return abort(401);
        }

        $experience = Experience::findOrFail($id);
        $experience->update($request->all());




        return (new ExperienceResource($experience))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('experience_delete')) {
            return abort(401);
        }

        $experience = Experience::findOrFail($id);
        $experience->delete();

        return response(null, 204);
    }
}
