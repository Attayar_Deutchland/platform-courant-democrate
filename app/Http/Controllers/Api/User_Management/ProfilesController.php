<?php

namespace App\Http\Controllers\Api\User_Management;

use App\Profile;
use App\Http\Controllers\Controller;
use App\Http\Resources\Profile as ProfileResource;
use App\Http\Requests\Admin\StoreProfilesRequest;
use App\Http\Requests\Admin\UpdateProfilesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class ProfilesController extends Controller
{
    public function index()
    {


        return new ProfileResource(Profile::with(['academic', 'experience', 'user'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('profile_view')) {
            return abort(401);
        }

        $profile = Profile::with(['academic', 'experience', 'user'])->findOrFail($id);

        return new ProfileResource($profile);
    }

    public function store(StoreProfilesRequest $request)
    {
        if (Gate::denies('profile_create')) {
            return abort(401);
        }

        $profile = Profile::create($request->all());
        $profile->academic()->sync($request->input('academic', []));
        $profile->experience()->sync($request->input('experience', []));
        if ($request->hasFile('photo')) {
            $profile->addMedia($request->file('photo'))->toMediaCollection('photo');
        }

        return (new ProfileResource($profile))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateProfilesRequest $request, $id)
    {
        if (Gate::denies('profile_edit')) {
            return abort(401);
        }

        $profile = Profile::findOrFail($id);
        $profile->update($request->all());
        $profile->academic()->sync($request->input('academic', []));
        $profile->experience()->sync($request->input('experience', []));
        if (! $request->input('photo') && $profile->getFirstMedia('photo')) {
            $profile->getFirstMedia('photo')->delete();
        }
        if ($request->hasFile('photo')) {
            $profile->addMedia($request->file('photo'))->toMediaCollection('photo');
        }

        return (new ProfileResource($profile))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('profile_delete')) {
            return abort(401);
        }

        $profile = Profile::findOrFail($id);
        $profile->delete();

        return response(null, 204);
    }
}
