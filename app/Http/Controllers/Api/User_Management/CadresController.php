<?php

namespace App\Http\Controllers\Api\User_Management;

use App\Cadre;
use App\Http\Controllers\Controller;
use App\Http\Resources\Cadre as CadreResource;
use App\Http\Requests\Admin\StoreCadresRequest;
use App\Http\Requests\Admin\UpdateCadresRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class CadresController extends Controller
{
    public function index()
    {


        return new CadreResource(Cadre::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('cadre_view')) {
            return abort(401);
        }

        $cadre = Cadre::with([])->findOrFail($id);

        return new CadreResource($cadre);
    }

    public function store(StoreCadresRequest $request)
    {
        if (Gate::denies('cadre_create')) {
            return abort(401);
        }

        $cadre = Cadre::create($request->all());



        return (new CadreResource($cadre))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateCadresRequest $request, $id)
    {
        if (Gate::denies('cadre_edit')) {
            return abort(401);
        }

        $cadre = Cadre::findOrFail($id);
        $cadre->update($request->all());




        return (new CadreResource($cadre))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('cadre_delete')) {
            return abort(401);
        }

        $cadre = Cadre::findOrFail($id);
        $cadre->delete();

        return response(null, 204);
    }
}
