<?php
namespace App\Http\Requests\Admin;

use App\Experience;
use Illuminate\Foundation\Http\FormRequest;

class StoreExperiencesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Experience::storeValidation($this);
    }
}
