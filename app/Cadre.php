<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cadre
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property string $date_beginn
 * @property string $date_end
 * @property tinyInteger $active
*/
class Cadre extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['title', 'description', 'date_beginn', 'date_end', 'active'];
    

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:65535|nullable',
            'date_beginn' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'date_end' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'active' => 'boolean|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:65535|nullable',
            'date_beginn' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'date_end' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'active' => 'boolean|nullable'
        ];
    }

    

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateBeginnAttribute($input)
    {
        if ($input) {
            $this->attributes['date_beginn'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateBeginnAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateEndAttribute($input)
    {
        if ($input) {
            $this->attributes['date_end'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateEndAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    
}
