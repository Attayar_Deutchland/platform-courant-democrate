<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class City
 *
 * @package App
 * @property string $name
 * @property double $lattitude
 * @property double $longitude
 * @property string $state
*/
class City extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['name', 'lattitude', 'longitude', 'state_id'];
    

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'lattitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'state_id' => 'integer|exists:states,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'lattitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'state_id' => 'integer|exists:states,id|max:4294967295|required'
        ];
    }

    

    
    
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id')->withTrashed();
    }
    
    
}
