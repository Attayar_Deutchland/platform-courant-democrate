<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Academic
 *
 * @package App
 * @property string $title
 * @property string $description
 * @property string $date_beginn
 * @property string $date_end
*/
class Academic extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['title', 'description', 'date_beginn', 'date_end'];
    

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:191|nullable',
            'date_beginn' => 'max:191|nullable',
            'date_end' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:191|nullable',
            'date_beginn' => 'max:191|nullable',
            'date_end' => 'max:191|nullable'
        ];
    }

    

    
    
    
}
