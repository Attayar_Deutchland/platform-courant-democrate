<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Experience
 *
 * @package App
 * @property string $title
 * @property string $description
 * @property string $date_beginn
 * @property string $date_end
*/
class Experience extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['title', 'description', 'date_beginn', 'date_end'];
    

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:191|nullable',
            'date_beginn' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'date_end' => 'date_format:' . config('app.date_format') . '|max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:191|nullable',
            'date_beginn' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'date_end' => 'date_format:' . config('app.date_format') . '|max:191|nullable'
        ];
    }

    

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateBeginnAttribute($input)
    {
        if ($input) {
            $this->attributes['date_beginn'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateBeginnAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateEndAttribute($input)
    {
        if ($input) {
            $this->attributes['date_end'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateEndAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    
}
