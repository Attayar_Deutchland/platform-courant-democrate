<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class State
 *
 * @package App
 * @property string $name
 * @property double $lattitude
 * @property double $longitude
 * @property string $country
*/
class State extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['name', 'lattitude', 'longitude', 'country_id'];
    

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'lattitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'country_id' => 'integer|exists:countries,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'lattitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'country_id' => 'integer|exists:countries,id|max:4294967295|required'
        ];
    }

    

    
    
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed();
    }
    
    
}
