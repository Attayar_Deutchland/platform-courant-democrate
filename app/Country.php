<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 *
 * @package App
 * @property string $shortcode
 * @property string $title
*/
class Country extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['shortcode', 'title'];
    

    public static function storeValidation($request)
    {
        return [
            'shortcode' => 'max:191|nullable',
            'title' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'shortcode' => 'max:191|nullable',
            'title' => 'max:191|nullable'
        ];
    }

    

    
    
    
}
