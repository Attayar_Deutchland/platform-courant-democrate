<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Role
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property string $logo
 * @property tinyInteger $active
 * @property string $date_beginn
 * @property string $date_end
 * @property string $country
 * @property string $state
 * @property string $city
*/
class Role extends Model implements HasMedia
{
    use HasMediaTrait;

    
    protected $fillable = ['title', 'description', 'active', 'date_beginn', 'date_end', 'country_id', 'state_id', 'city_id'];
    protected $appends = ['logo', 'logo_link'];
    protected $with = ['media'];
    

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'cadre' => 'array|nullable',
            'cadre.*' => 'integer|exists:cadres,id|max:4294967295|nullable',
            'description' => 'max:65535|nullable',
            'permission' => 'array|required',
            'permission.*' => 'integer|exists:permissions,id|max:4294967295|required',
            'logo' => 'file|image|nullable',
            'active' => 'boolean|nullable',
            'date_beginn' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'date_end' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'country_id' => 'integer|exists:countries,id|max:4294967295|nullable',
            'state_id' => 'integer|exists:states,id|max:4294967295|nullable',
            'city_id' => 'integer|exists:cities,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'cadre' => 'array|nullable',
            'cadre.*' => 'integer|exists:cadres,id|max:4294967295|nullable',
            'description' => 'max:65535|nullable',
            'permission' => 'array|required',
            'permission.*' => 'integer|exists:permissions,id|max:4294967295|required',
            'logo' => 'nullable',
            'active' => 'boolean|nullable',
            'date_beginn' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'date_end' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'country_id' => 'integer|exists:countries,id|max:4294967295|nullable',
            'state_id' => 'integer|exists:states,id|max:4294967295|nullable',
            'city_id' => 'integer|exists:cities,id|max:4294967295|nullable'
        ];
    }

    

    public function getLogoAttribute()
    {
        return $this->getFirstMedia('logo');
    }

    /**
     * @return string
     */
    public function getLogoLinkAttribute()
    {
        $file = $this->getFirstMedia('logo');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateBeginnAttribute($input)
    {
        if ($input) {
            $this->attributes['date_beginn'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateBeginnAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateEndAttribute($input)
    {
        if ($input) {
            $this->attributes['date_end'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDateEndAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    public function cadre()
    {
        return $this->belongsToMany(Cadre::class, 'cadre_role')->withTrashed();
    }
    
    public function permission()
    {
        return $this->belongsToMany(Permission::class, 'permission_role');
    }
    
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed();
    }
    
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id')->withTrashed();
    }
    
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id')->withTrashed();
    }
    
    
}
