import Vue from 'vue'
import Vuex from 'vuex'
import Alert from './modules/alert'
import ChangePassword from './modules/change_password'
import Rules from './modules/rules'
import PermissionsIndex from './modules/Permissions'
import PermissionsSingle from './modules/Permissions/single'
import RolesIndex from './modules/Roles'
import RolesSingle from './modules/Roles/single'
import UsersIndex from './modules/Users'
import UsersSingle from './modules/Users/single'
import CadresIndex from './modules/Cadres'
import CadresSingle from './modules/Cadres/single'
import CountriesIndex from './modules/Countries'
import CountriesSingle from './modules/Countries/single'
import StatesIndex from './modules/States'
import StatesSingle from './modules/States/single'
import CitiesIndex from './modules/Cities'
import CitiesSingle from './modules/Cities/single'
import AcademicsIndex from './modules/Academics'
import AcademicsSingle from './modules/Academics/single'
import ExperiencesIndex from './modules/Experiences'
import ExperiencesSingle from './modules/Experiences/single'
import ProfilesIndex from './modules/Profiles'
import ProfilesSingle from './modules/Profiles/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        ChangePassword,
        Rules,
        PermissionsIndex,
        PermissionsSingle,
        RolesIndex,
        RolesSingle,
        UsersIndex,
        UsersSingle,
        CadresIndex,
        CadresSingle,
        CountriesIndex,
        CountriesSingle,
        StatesIndex,
        StatesSingle,
        CitiesIndex,
        CitiesSingle,
        AcademicsIndex,
        AcademicsSingle,
        ExperiencesIndex,
        ExperiencesSingle,
        ProfilesIndex,
        ProfilesSingle,
    },
    strict: debug,
})
