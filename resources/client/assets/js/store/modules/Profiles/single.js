function initialState() {
    return {
        item: {
            id: null,
            firstname: null,
            lastname: null,
            photo: null,
            date_of_birthday: null,
            adresse: null,
            date_inscription: null,
            sexe: null,
            status: null,
            academic: [],
            experience: [],
            user: null,
        },
        academicsAll: [],
        experiencesAll: [],
        usersAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    academicsAll: state => state.academicsAll,
    experiencesAll: state => state.experiencesAll,
    usersAll: state => state.usersAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.photo === null) {
                params.delete('photo');
            }
            if (_.isEmpty(state.item.academic)) {
                params.delete('academic')
            } else {
                for (let index in state.item.academic) {
                    params.set('academic['+index+']', state.item.academic[index].id)
                }
            }
            if (_.isEmpty(state.item.experience)) {
                params.delete('experience')
            } else {
                for (let index in state.item.experience) {
                    params.set('experience['+index+']', state.item.experience[index].id)
                }
            }
            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }

            axios.post('/api/v1/profiles', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.photo === null) {
                params.delete('photo');
            }
            if (_.isEmpty(state.item.academic)) {
                params.delete('academic')
            } else {
                for (let index in state.item.academic) {
                    params.set('academic['+index+']', state.item.academic[index].id)
                }
            }
            if (_.isEmpty(state.item.experience)) {
                params.delete('experience')
            } else {
                for (let index in state.item.experience) {
                    params.set('experience['+index+']', state.item.experience[index].id)
                }
            }
            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }

            axios.post('/api/v1/profiles/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/profiles/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchAcademicsAll')
    dispatch('fetchExperiencesAll')
    dispatch('fetchUsersAll')
    },
    fetchAcademicsAll({ commit }) {
        axios.get('/api/v1/academics')
            .then(response => {
                commit('setAcademicsAll', response.data.data)
            })
    },
    fetchExperiencesAll({ commit }) {
        axios.get('/api/v1/experiences')
            .then(response => {
                commit('setExperiencesAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    setFirstname({ commit }, value) {
        commit('setFirstname', value)
    },
    setLastname({ commit }, value) {
        commit('setLastname', value)
    },
    setPhoto({ commit }, value) {
        commit('setPhoto', value)
    },
    
    setDate_of_birthday({ commit }, value) {
        commit('setDate_of_birthday', value)
    },
    setAdresse({ commit }, value) {
        commit('setAdresse', value)
    },
    setDate_inscription({ commit }, value) {
        commit('setDate_inscription', value)
    },
    setSexe({ commit }, value) {
        commit('setSexe', value)
    },
    setStatus({ commit }, value) {
        commit('setStatus', value)
    },
    setAcademic({ commit }, value) {
        commit('setAcademic', value)
    },
    setExperience({ commit }, value) {
        commit('setExperience', value)
    },
    setUser({ commit }, value) {
        commit('setUser', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setFirstname(state, value) {
        state.item.firstname = value
    },
    setLastname(state, value) {
        state.item.lastname = value
    },
    setPhoto(state, value) {
        state.item.photo = value
    },
    setDate_of_birthday(state, value) {
        state.item.date_of_birthday = value
    },
    setAdresse(state, value) {
        state.item.adresse = value
    },
    setDate_inscription(state, value) {
        state.item.date_inscription = value
    },
    setSexe(state, value) {
        state.item.sexe = value
    },
    setStatus(state, value) {
        state.item.status = value
    },
    setAcademic(state, value) {
        state.item.academic = value
    },
    setExperience(state, value) {
        state.item.experience = value
    },
    setUser(state, value) {
        state.item.user = value
    },
    setAcademicsAll(state, value) {
        state.academicsAll = value
    },
    setExperiencesAll(state, value) {
        state.experiencesAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
