function initialState() {
    return {
        item: {
            id: null,
            title: null,
            cadre: [],
            description: null,
            permission: [],
            logo: null,
            active: false,
            date_beginn: null,
            date_end: null,
            country: null,
            state: null,
            city: null,
        },
        cadresAll: [],
        permissionsAll: [],
        countriesAll: [],
        statesAll: [],
        citiesAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    cadresAll: state => state.cadresAll,
    permissionsAll: state => state.permissionsAll,
    countriesAll: state => state.countriesAll,
    statesAll: state => state.statesAll,
    citiesAll: state => state.citiesAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.cadre)) {
                params.delete('cadre')
            } else {
                for (let index in state.item.cadre) {
                    params.set('cadre['+index+']', state.item.cadre[index].id)
                }
            }
            if (_.isEmpty(state.item.permission)) {
                params.delete('permission')
            } else {
                for (let index in state.item.permission) {
                    params.set('permission['+index+']', state.item.permission[index].id)
                }
            }
            if (state.item.logo === null) {
                params.delete('logo');
            }
            params.set('active', state.item.active ? 1 : 0)
            if (_.isEmpty(state.item.country)) {
                params.set('country_id', '')
            } else {
                params.set('country_id', state.item.country.id)
            }
            if (_.isEmpty(state.item.state)) {
                params.set('state_id', '')
            } else {
                params.set('state_id', state.item.state.id)
            }
            if (_.isEmpty(state.item.city)) {
                params.set('city_id', '')
            } else {
                params.set('city_id', state.item.city.id)
            }

            axios.post('/api/v1/roles', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.cadre)) {
                params.delete('cadre')
            } else {
                for (let index in state.item.cadre) {
                    params.set('cadre['+index+']', state.item.cadre[index].id)
                }
            }
            if (_.isEmpty(state.item.permission)) {
                params.delete('permission')
            } else {
                for (let index in state.item.permission) {
                    params.set('permission['+index+']', state.item.permission[index].id)
                }
            }
            if (state.item.logo === null) {
                params.delete('logo');
            }
            params.set('active', state.item.active ? 1 : 0)
            if (_.isEmpty(state.item.country)) {
                params.set('country_id', '')
            } else {
                params.set('country_id', state.item.country.id)
            }
            if (_.isEmpty(state.item.state)) {
                params.set('state_id', '')
            } else {
                params.set('state_id', state.item.state.id)
            }
            if (_.isEmpty(state.item.city)) {
                params.set('city_id', '')
            } else {
                params.set('city_id', state.item.city.id)
            }

            axios.post('/api/v1/roles/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/roles/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchCadresAll')
    dispatch('fetchPermissionsAll')
    dispatch('fetchCountriesAll')
    dispatch('fetchStatesAll')
    dispatch('fetchCitiesAll')
    },
    fetchCadresAll({ commit }) {
        axios.get('/api/v1/cadres')
            .then(response => {
                commit('setCadresAll', response.data.data)
            })
    },
    fetchPermissionsAll({ commit }) {
        axios.get('/api/v1/permissions')
            .then(response => {
                commit('setPermissionsAll', response.data.data)
            })
    },
    fetchCountriesAll({ commit }) {
        axios.get('/api/v1/countries')
            .then(response => {
                commit('setCountriesAll', response.data.data)
            })
    },
    fetchStatesAll({ commit }) {
        axios.get('/api/v1/states')
            .then(response => {
                commit('setStatesAll', response.data.data)
            })
    },
    fetchCitiesAll({ commit }) {
        axios.get('/api/v1/cities')
            .then(response => {
                commit('setCitiesAll', response.data.data)
            })
    },
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setCadre({ commit }, value) {
        commit('setCadre', value)
    },
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setPermission({ commit }, value) {
        commit('setPermission', value)
    },
    setLogo({ commit }, value) {
        commit('setLogo', value)
    },
    
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    setDate_beginn({ commit }, value) {
        commit('setDate_beginn', value)
    },
    setDate_end({ commit }, value) {
        commit('setDate_end', value)
    },
    setCountry({ commit }, value) {
        commit('setCountry', value)
    },
    setState({ commit }, value) {
        commit('setState', value)
    },
    setCity({ commit }, value) {
        commit('setCity', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setCadre(state, value) {
        state.item.cadre = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setPermission(state, value) {
        state.item.permission = value
    },
    setLogo(state, value) {
        state.item.logo = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    setDate_beginn(state, value) {
        state.item.date_beginn = value
    },
    setDate_end(state, value) {
        state.item.date_end = value
    },
    setCountry(state, value) {
        state.item.country = value
    },
    setState(state, value) {
        state.item.state = value
    },
    setCity(state, value) {
        state.item.city = value
    },
    setCadresAll(state, value) {
        state.cadresAll = value
    },
    setPermissionsAll(state, value) {
        state.permissionsAll = value
    },
    setCountriesAll(state, value) {
        state.countriesAll = value
    },
    setStatesAll(state, value) {
        state.statesAll = value
    },
    setCitiesAll(state, value) {
        state.citiesAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
