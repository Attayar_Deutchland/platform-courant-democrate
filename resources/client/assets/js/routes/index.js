import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '../components/ChangePassword.vue'
import PermissionsIndex from '../components/cruds/Permissions/Index.vue'
import PermissionsCreate from '../components/cruds/Permissions/Create.vue'
import PermissionsShow from '../components/cruds/Permissions/Show.vue'
import PermissionsEdit from '../components/cruds/Permissions/Edit.vue'
import RolesIndex from '../components/cruds/Roles/Index.vue'
import RolesCreate from '../components/cruds/Roles/Create.vue'
import RolesShow from '../components/cruds/Roles/Show.vue'
import RolesEdit from '../components/cruds/Roles/Edit.vue'
import UsersIndex from '../components/cruds/Users/Index.vue'
import UsersCreate from '../components/cruds/Users/Create.vue'
import UsersShow from '../components/cruds/Users/Show.vue'
import UsersEdit from '../components/cruds/Users/Edit.vue'
import CadresIndex from '../components/cruds/Cadres/Index.vue'
import CadresCreate from '../components/cruds/Cadres/Create.vue'
import CadresShow from '../components/cruds/Cadres/Show.vue'
import CadresEdit from '../components/cruds/Cadres/Edit.vue'
import CountriesIndex from '../components/cruds/Countries/Index.vue'
import CountriesCreate from '../components/cruds/Countries/Create.vue'
import CountriesShow from '../components/cruds/Countries/Show.vue'
import CountriesEdit from '../components/cruds/Countries/Edit.vue'
import StatesIndex from '../components/cruds/States/Index.vue'
import StatesCreate from '../components/cruds/States/Create.vue'
import StatesShow from '../components/cruds/States/Show.vue'
import StatesEdit from '../components/cruds/States/Edit.vue'
import CitiesIndex from '../components/cruds/Cities/Index.vue'
import CitiesCreate from '../components/cruds/Cities/Create.vue'
import CitiesShow from '../components/cruds/Cities/Show.vue'
import CitiesEdit from '../components/cruds/Cities/Edit.vue'
import AcademicsIndex from '../components/cruds/Academics/Index.vue'
import AcademicsCreate from '../components/cruds/Academics/Create.vue'
import AcademicsShow from '../components/cruds/Academics/Show.vue'
import AcademicsEdit from '../components/cruds/Academics/Edit.vue'
import ExperiencesIndex from '../components/cruds/Experiences/Index.vue'
import ExperiencesCreate from '../components/cruds/Experiences/Create.vue'
import ExperiencesShow from '../components/cruds/Experiences/Show.vue'
import ExperiencesEdit from '../components/cruds/Experiences/Edit.vue'
import ProfilesIndex from '../components/cruds/Profiles/Index.vue'
import ProfilesCreate from '../components/cruds/Profiles/Create.vue'
import ProfilesShow from '../components/cruds/Profiles/Show.vue'
import ProfilesEdit from '../components/cruds/Profiles/Edit.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/change-password', component: ChangePassword, name: 'auth.change_password' },
    { path: '/permissions', component: PermissionsIndex, name: 'permissions.index' },
    { path: '/permissions/create', component: PermissionsCreate, name: 'permissions.create' },
    { path: '/permissions/:id', component: PermissionsShow, name: 'permissions.show' },
    { path: '/permissions/:id/edit', component: PermissionsEdit, name: 'permissions.edit' },
    { path: '/roles', component: RolesIndex, name: 'roles.index' },
    { path: '/roles/create', component: RolesCreate, name: 'roles.create' },
    { path: '/roles/:id', component: RolesShow, name: 'roles.show' },
    { path: '/roles/:id/edit', component: RolesEdit, name: 'roles.edit' },
    { path: '/users', component: UsersIndex, name: 'users.index' },
    { path: '/users/create', component: UsersCreate, name: 'users.create' },
    { path: '/users/:id', component: UsersShow, name: 'users.show' },
    { path: '/users/:id/edit', component: UsersEdit, name: 'users.edit' },
    { path: '/cadres', component: CadresIndex, name: 'cadres.index' },
    { path: '/cadres/create', component: CadresCreate, name: 'cadres.create' },
    { path: '/cadres/:id', component: CadresShow, name: 'cadres.show' },
    { path: '/cadres/:id/edit', component: CadresEdit, name: 'cadres.edit' },
    { path: '/countries', component: CountriesIndex, name: 'countries.index' },
    { path: '/countries/create', component: CountriesCreate, name: 'countries.create' },
    { path: '/countries/:id', component: CountriesShow, name: 'countries.show' },
    { path: '/countries/:id/edit', component: CountriesEdit, name: 'countries.edit' },
    { path: '/states', component: StatesIndex, name: 'states.index' },
    { path: '/states/create', component: StatesCreate, name: 'states.create' },
    { path: '/states/:id', component: StatesShow, name: 'states.show' },
    { path: '/states/:id/edit', component: StatesEdit, name: 'states.edit' },
    { path: '/cities', component: CitiesIndex, name: 'cities.index' },
    { path: '/cities/create', component: CitiesCreate, name: 'cities.create' },
    { path: '/cities/:id', component: CitiesShow, name: 'cities.show' },
    { path: '/cities/:id/edit', component: CitiesEdit, name: 'cities.edit' },
    { path: '/academics', component: AcademicsIndex, name: 'academics.index' },
    { path: '/academics/create', component: AcademicsCreate, name: 'academics.create' },
    { path: '/academics/:id', component: AcademicsShow, name: 'academics.show' },
    { path: '/academics/:id/edit', component: AcademicsEdit, name: 'academics.edit' },
    { path: '/experiences', component: ExperiencesIndex, name: 'experiences.index' },
    { path: '/experiences/create', component: ExperiencesCreate, name: 'experiences.create' },
    { path: '/experiences/:id', component: ExperiencesShow, name: 'experiences.show' },
    { path: '/experiences/:id/edit', component: ExperiencesEdit, name: 'experiences.edit' },
    { path: '/profiles', component: ProfilesIndex, name: 'profiles.index' },
    { path: '/profiles/create', component: ProfilesCreate, name: 'profiles.create' },
    { path: '/profiles/:id', component: ProfilesShow, name: 'profiles.show' },
    { path: '/profiles/:id/edit', component: ProfilesEdit, name: 'profiles.edit' },
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
