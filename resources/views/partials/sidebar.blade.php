@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('quickadmin.qa_dashboard')</span>
                </a>
            </li>

            <li class="treeview" v-if="$can('user_management_access')">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>@lang('quickadmin.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('permission_access')">
                        <router-link :to="{ name: 'permissions.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.permissions.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('role_access')">
                        <router-link :to="{ name: 'roles.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.roles.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('user_access')">
                        <router-link :to="{ name: 'users.index' }">
                            <i class="fa fa-user"></i>
                            <span>@lang('quickadmin.users.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('cadre_access')">
                        <router-link :to="{ name: 'cadres.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.cadre.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('country_access')">
                        <router-link :to="{ name: 'countries.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.countries.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('state_access')">
                        <router-link :to="{ name: 'states.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.states.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('city_access')">
                        <router-link :to="{ name: 'cities.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.city.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('academic_access')">
                        <router-link :to="{ name: 'academics.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.academic.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('experience_access')">
                        <router-link :to="{ name: 'experiences.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.experience.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('profile_access')">
                        <router-link :to="{ name: 'profiles.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.profiles.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>

            <li>
                <router-link :to="{ name: 'auth.change_password' }">
                    <i class="fa fa-key"></i>
                    <span class="title">@lang('quickadmin.qa_change_password')</span>
                </router-link>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
